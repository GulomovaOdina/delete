-- Delete all rental records associated with the film
DELETE FROM rental
WHERE inventory_id IN (
    SELECT inventory_id
    FROM inventory
    WHERE film_id = (SELECT film_id FROM film WHERE title = 'Inception')
);

-- Delete the film from the inventory
DELETE FROM inventory
WHERE film_id = (SELECT film_id FROM film WHERE title = 'Inception');

-- Delete records related to the customer from all tables except "Customer" and "Inventory"
DELETE FROM address
WHERE address_id IN (
    SELECT address_id
    FROM customer
    WHERE first_name = 'John' AND last_name = 'Doe' -- Assuming this is your fictional name used in the previous update
);

DELETE FROM payment
WHERE customer_id IN (
    SELECT customer_id
    FROM customer
    WHERE first_name = 'John' AND last_name = 'Doe'
);

DELETE FROM rental
WHERE customer_id IN (
    SELECT customer_id
    FROM customer
    WHERE first_name = 'John' AND last_name = 'Doe'
);

DELETE FROM film_actor
WHERE film_id IN (
    SELECT film_id
    FROM film
    WHERE title = 'Inception'
);

DELETE FROM film
WHERE title = 'Inception';
